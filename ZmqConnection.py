#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Apr 11, 2013

@author: Seung-Jin
""" 

import zmq
import socket
import sys

from MsgCompress import *

zmqSocketTypes = {"ZMQ_PAIR" : 0,
                  "ZMQ_PUB" : 1,
                  "ZMQ_SUB" : 2,
                  "ZMQ_REQ" : 3,
                  "ZMQ_REP" : 4,
                  "ZMQ_DEALER" : 5,
                  "ZMQ_ROUTER" : 6,
                  "ZMQ_PULL" : 7,
                  "ZMQ_PUSH" : 8,
                  "ZMQ_XPUB" : 9,
                  "ZMQ_XSUB" : 10}

"""
Created on 04/11/2013

ZMQ connection wrapper class

@author Seung-Jin Sul
@contact ssul@lbl.gov
"""
class ZmqConnection(object):
    
    __hostName = None
    __portNum = None
    __homeAddress = None
    __zSocket = None
    
    def __init__(self, hostName, socketType, port=None):
        self.__hostName = hostName
        
        context = zmq.Context()
        self.__zSocket = context.socket(zmqSocketTypes[socketType])
        self.__hostName = hostName
        ipAddress = socket.gethostbyname(self.__hostName)
        interface = "tcp://%s" % (ipAddress)
        print "interface = %s" % (interface)
        
        if port:
            self.__portNum = int(port)
        else: 
            self.__portNum = self.__zSocket.bind_to_random_port(interface)
        
        self.__homeAddress = "%s:%i" % (interface, int(port))
    
    def close(self):
        print "Connection closed."
        self.__zSocket.close()
    
    def bind(self):
        self.__zSocket.bind(self.__homeAddress)
        print "server is running on: %s" % (self.__homeAddress)
    
    def getHomeAddress(self):
        return self.__homeAddress
    
    def getSocket(self):
        return self.__zSocket
    
    def recv(self):
        msgZipped = self.__zSocket.recv()
        return zloads(msgZipped)
    
    def send(self, msgContainer):
        msgZipped = zdumps(msgContainer)
        self.__zSocket.send(msgZipped)
        
    def connect(self):
        self.__zSocket.connect(self.__homeAddress)
    
## EOF