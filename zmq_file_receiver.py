#! /usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) 2012 NERSC, LBL

import socket
import sys

from ZmqConnection import *


def recvAndAck(zmqConn):
    while True:
        msg = zmqConn.recv()
        print "Got msg:", msg
        zmqConn.send({"rep": "OK"})


def main(argv):
    zmqConn = ZmqConnection(socket.gethostname(), "ZMQ_REP", 60001)
    print "Try to bind to %s" % (zmqConn.getHomeAddress())
    zmqConn.bind()
    print "Binded"
    
    try:
        recvAndAck(zmqConn)
    except KeyboardInterrupt:
        zmqConn.close()
        return 1
        

if __name__ == "__main__":
    sys.exit(main(sys.argv))

## EOF
