#! /usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) 2012 NERSC, LBL

"""


"""

import os
import sys
import getopt
import time

from ResourceUsage import *
from ZmqConnection import *

"""
send info to the server

@param servName
@param servPort
@param sendInterval
@param pid
"""
def send(zmqConn, servName, servPort, sendInterval, pid):
    while 1:
        msgContainer = {}
        msgContainer["host"] = os.uname()[1]
        msgContainer["pid"] = pid
        msgContainer["cpu"] = get_cpu_load(int(pid))
        msgContainer["mem"] = get_memory_usage(int(pid))
        msgContainer["runtime"] = get_runtime(int(pid))

        zmqConn.send(msgContainer)
        retMsg = zmqConn.recv()
        print "server replied:", (retMsg)

        time.sleep(int(sendInterval))


def usage():
    print "Usage: zmq_file_sender.py -s <server> -p <port> -i <pid> -d <interval>"


def main(argv):
    """
    Parse the command line inputs and call send

    @param argv: list of arguments
    @type argv: list of strings
    """

    servname = ''
    servport = ''
    pid = ''
    interval = ''

    try:
        opts, args = getopt.getopt(argv[1:], "hs:p:i:d:", ["server=", "port=", "pid=", "interval="])
    except getopt.GetoptError:
        usage()
        return 1
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-s", "--server"):
            servname = arg
        elif opt in ("-p", "--port"):
            servport = arg
        elif opt in ("-i", "--pid"):
            pid = arg
        elif opt in ("-d", "--interval"):
            interval = arg
        else:
            usage()
            return 1
    
    zmqConn = ZmqConnection(servname, "ZMQ_REQ", servport)
    print "Try to connect to %s" % (zmqConn.getHomeAddress())
    zmqConn.connect()
    print "Connected"
    
    try:
        send(zmqConn, servname, servport, interval, pid)    
    except KeyboardInterrupt:
        zmqConn.close()
        return 1

if __name__ == "__main__":
    sys.exit(main(sys.argv))

## EOF